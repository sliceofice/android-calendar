package com.example.calendar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.findNavController
import androidx.room.Room
import com.example.calendar.database.AppDatabase
import com.example.calendar.database.DayEventsDao
import com.example.calendar.database.EventRepository
import com.example.calendar.database.entities.DayEvents
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.Date

class AddEvent : Fragment() {

    private var dao: DayEventsDao? = null
    private var activeDate: Date = Date()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.supportFragmentManager?.setFragmentResultListener(
            "fromCalendar",
            this
        ) { key, bundle ->
            val result = bundle.getString("actual_date")
            activeDate = Date(result)
            val res = activeDate.toString()
            setFragmentResult("fromAddEvent", bundleOf("actual_date" to res))
        }
        setFragmentResultListener(
            "fromDayDetails"
        ) { key, bundle ->
            val result = bundle.getString("actual_date")
            activeDate = Date(result)
            val res = activeDate.toString()
            setFragmentResult("fromAddEvent", bundleOf("actual_date" to res))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_event, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dao = init_db()

        val button_back: Button = view.findViewById(R.id.button_back)
        button_back.setOnClickListener {
            view.findNavController().navigate(R.id.action_addEvent_to_dayDetails)
        }
        val button_save: Button = view.findViewById(R.id.button_save)
        val tiet_top1: com.google.android.material.textfield.TextInputEditText =
            view.findViewById(R.id.tiet_top1)
        val tiet_top2: com.google.android.material.textfield.TextInputEditText =
            view.findViewById(R.id.tiet_top2)
        button_save.setOnClickListener {
            var text = tiet_top1.text.toString()
            if (text == "") {
                text = "-"
            }
            val sum: Float
            if (tiet_top2.text.toString() == "") {
                sum = 0.toFloat()
            } else {
                sum = tiet_top2.text.toString().toFloat()
            }
            dao?.let { it2 ->
                runBlocking {
                    val q = launch {
                        EventRepository(it2).insertNewEventData(
                            DayEvents(
                                0,
                                activeDate.date.toLong(),
                                activeDate.month.toLong() + 1,
                                activeDate.year.toLong() + 1900,
                                text,
                                sum
                            )
                        )
                    }
                    q.join()
                    val result = "updated"
                    activity?.supportFragmentManager?.setFragmentResult(
                        "toCalendar",
                        bundleOf("status" to result)
                    )
                    view.findNavController().navigate(R.id.action_addEvent_to_dayDetails)
                }
            }
        }
    }

    fun init_db(): DayEventsDao? {
        view?.let {
            val db = Room.databaseBuilder(
                it.context,
                AppDatabase::class.java, "day_events_database"
            ).build()
            val dao = db.getDayEventDao()
            return dao
        }
        return null
    }
}
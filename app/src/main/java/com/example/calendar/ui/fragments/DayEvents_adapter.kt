package com.example.calendar

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.compose.ui.graphics.Color
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.calendar.database.EventsTuple
import java.util.Date

class DayEventsAdapter(private val itemClickListener: ItemClickListener) :
    RecyclerView.Adapter<DayEventsAdapter.DayEventsViewHolder>() {

    private var dayEvents: MutableList<EventsTuple> = mutableListOf()

    interface ItemClickListener {
        fun onItemClick(dayEvent: EventsTuple)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayEventsAdapter.DayEventsViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.event_layout, parent, false)
        return DayEventsViewHolder(itemView, itemClickListener)
    }

    override fun getItemCount(): Int {
        return dayEvents.size
    }

    override fun onBindViewHolder(
        holder: DayEventsViewHolder,
        position: Int
    ) {
        dayEvents.let {
            val dayEvent: EventsTuple = it[position]
            holder.bind(dayEvent)
        }
    }

    fun addData(newData: List<EventsTuple>?) {
        newData?.let {
            dayEvents = mutableListOf()
            dayEvents.addAll(it)
        }
        notifyDataSetChanged()
    }

    class DayEventsViewHolder(
        var view: View,
        val itemClickListener: ItemClickListener
    ) : RecyclerView.ViewHolder(view) {
        var tvSum: TextView
        var tvText: TextView
        var button_del: Button

        init {
            tvSum = itemView.findViewById(R.id.tvSum)
            tvText = itemView.findViewById(R.id.tvText)
            button_del = itemView.findViewById(R.id.button_del)
        }

        fun bind(dayEvent: EventsTuple) {
            button_del.setOnClickListener {
                itemClickListener.onItemClick(dayEvent)
            }
            val sum = dayEvent.sumChange
            tvSum.text = sum.toString()
            if (sum < 0) {
                tvSum.setTextColor(ContextCompat.getColor(view.context, R.color.red))
            }
            if (sum > 0) {
                tvSum.setTextColor(ContextCompat.getColor(view.context, R.color.green))
            }
            if (sum == 0.toFloat()) {
                tvSum.setTextColor(ContextCompat.getColor(view.context, R.color.black))
            }
            tvText.text = dayEvent.eventText
        }
    }

}

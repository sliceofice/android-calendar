package com.example.calendar

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.calendar.database.SumsByDaysTuple
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

class GvAdapter(private val itemClickListener: ItemClickListener) :
    RecyclerView.Adapter<GvAdapter.GvViewHolder>() {

    var date: Date = Date() // выбранная дата
    private var daysEvents: MutableList<SumsByDaysTuple> = mutableListOf()
    private var days: MutableList<Date?> = fill_calendar()

    interface ItemClickListener {
        fun onItemClick(day: Date)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GvAdapter.GvViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.day_layout, parent, false)
        return GvViewHolder(itemView, itemClickListener)
    }

    override fun onBindViewHolder(
        holder: GvViewHolder,
        position: Int
    ) {
        val day: Date? = days[position]
        holder.bind(day, date, daysEvents)
    }

    override fun getItemCount(): Int {
        return days.size
    }

    fun addData(newData: List<SumsByDaysTuple>?) {
        newData?.let {
            daysEvents = mutableListOf()
            daysEvents.addAll(it)
        }
        notifyDataSetChanged()
    }

    fun setDateCal(newDate: Date) {
        date = newDate
        days = fill_calendar()
        notifyDataSetChanged()
    }

    private fun fill_calendar(): MutableList<Date?> {
        val dayss: MutableList<Date?> = mutableListOf()
        val days_in_month = get_number_days_of_month(date.month, date.year + 1900)
        val first_day = get_first_day_of_month(date.month, date.year + 1900)
        when (first_day) {
            "Tuesday" -> add_blank(1, dayss)
            "Wednesday" -> add_blank(2, dayss)
            "Thursday" -> add_blank(3, dayss)
            "Friday" -> add_blank(4, dayss)
            "Saturday" -> add_blank(5, dayss)
            "Sunday" -> add_blank(6, dayss)
            else -> {
                print("")
            }
        }
        for (i in 1..days_in_month) {
            dayss.add(Date(date.year, date.month, i))
        }
        return dayss
    }

    private fun get_day_of_week(d: Date): String {
        val sdf = SimpleDateFormat("EEEE")
        val dayOfTheWeek: String = sdf.format(d)
        return dayOfTheWeek
    }

    private fun get_number_days_of_month(month: Int, year: Int): Int {
        val cal: Calendar = Calendar.getInstance()
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE))
        val endDay: Date = cal.getTime()
        return endDay.getDate()
    }

    private fun get_first_day_of_month(month: Int, year: Int): String {
        val cal: Calendar = Calendar.getInstance()
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.DAY_OF_MONTH, 1)
        val firstDay: Date = cal.getTime()
        return get_day_of_week(firstDay)
    }

    private fun add_blank(num: Int, days: MutableList<Date?>): MutableList<Date?> {
        for (i in 1..num) {
            days.add(null)
        }
        return days
    }

    class GvViewHolder(
        var view: View,
        val itemClickListener: ItemClickListener
    ) : RecyclerView.ViewHolder(view) {
        var tvDay: TextView
        var vDay: ConstraintLayout

        fun set_day_style(view: View, dayInfo: Date?, date: Date, daysEvents: MutableList<SumsByDaysTuple>): Drawable? {
            var border = false
            var color = ""
            var result: Drawable? = null
            for (i in daysEvents.indices) {
                if (daysEvents[i].dayDate.toInt() == dayInfo?.date) {
                    color = "no"
                    Log.d("TAG1235", daysEvents[i].toString())
                    if (daysEvents[i].sums > 0) {
                        color = "green"
                    }
                    if (daysEvents[i].sums < 0) {
                        color = "red"
                    }
                    if (daysEvents[i].sums == 0.toFloat()) {
                        color = "yelow"
                    }
                }
                Log.d("TAG1235", (color+'-'+dayInfo?.date).toString())
            }

            if (dayInfo == Date(Date().year, Date().month, Date().date)) {
                color = "blue"
            }
            if (dayInfo == date) {
                border = true
            }
            if (border) {
                when (color) {
                    "blue" -> result =
                        ContextCompat.getDrawable(view.context, R.drawable.day_border_blue_bg_blue)
                    "green" -> result =
                        ContextCompat.getDrawable(view.context, R.drawable.day_border_blue_bg_green)
                    "red" -> result =
                        ContextCompat.getDrawable(view.context, R.drawable.day_border_blue_bg_red)
                    "yelow" -> result =
                        ContextCompat.getDrawable(view.context, R.drawable.day_border_blue_bg_yelow)
                    else -> {
                        result = ContextCompat.getDrawable(view.context, R.drawable.day_border_blue)
                    }
                }
            } else {
                when (color) {
                    "blue" -> result =
                        ContextCompat.getDrawable(view.context, R.drawable.day_bg_blue)
                    "green" -> result =
                        ContextCompat.getDrawable(view.context, R.drawable.day_bg_green)
                    "red" -> result =
                        ContextCompat.getDrawable(view.context, R.drawable.day_bg_red)
                    "yelow" -> result =
                        ContextCompat.getDrawable(view.context, R.drawable.day_bg_yelow)
                    else -> {}
                }
            }
            return result
        }

        init {
            tvDay = itemView.findViewById(R.id.textView_day)
            vDay = itemView.findViewById(R.id.v_day)
        }

        fun bind(dayInfo: Date?, date: Date, daysEvents: MutableList<SumsByDaysTuple>) {
            itemView.setOnClickListener {
                dayInfo?.let { itemClickListener.onItemClick(it) }
            }
            dayInfo?.let {
                tvDay.text = it.date.toString()
                vDay.background = set_day_style(view, dayInfo, date, daysEvents)
                return
            }
            tvDay.text = ""

        }
    }

}

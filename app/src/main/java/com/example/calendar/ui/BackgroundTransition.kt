package com.example.calendar.ui

import android.graphics.drawable.TransitionDrawable
import android.view.ViewGroup
import java.util.Timer
import kotlin.concurrent.schedule

class BackgroundTransition(layout: ViewGroup, private val duration: Int) {
    private val transition: TransitionDrawable = layout.getBackground() as TransitionDrawable

    fun animate() {
        startTrans(transition, duration)
    }

    private fun startTrans(transition: TransitionDrawable, duration: Int) {
        transition.startTransition(duration)
        Timer().schedule(duration.toLong()) {
            transition.reverseTransition(duration)
        }
        Timer().schedule((duration*2).toLong()) {
            startTrans(transition, duration)
        }
    }
}
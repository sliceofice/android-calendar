package com.example.calendar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class HeaderAdapter: RecyclerView.Adapter<HeaderAdapter.HeaderViewHolder>() {

    private var days: MutableList<String> = mutableListOf("ПН","ВТ","СР","ЧТ","ПТ","СБ","ВС")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeaderViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.day_of_week_layout, parent, false)
        return HeaderViewHolder(itemView)
    }

    override fun onBindViewHolder(
        holder: HeaderViewHolder,
        position: Int
    ) {
        val day: String = days[position]
        holder.bind(day)
    }

    override fun getItemCount(): Int {
        return days.size
    }

    class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvDay: TextView

        init {
            tvDay = itemView.findViewById(R.id.textView_day_of_week)
        }

        fun bind(dayInfo: String) {
            tvDay.setText(dayInfo)

        }
    }

}

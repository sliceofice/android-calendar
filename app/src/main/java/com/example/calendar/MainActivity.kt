package com.example.calendar

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.room.Room
import com.example.calendar.database.AppDatabase
import com.example.calendar.database.EventRepository
import com.example.calendar.database.entities.DayEvents
import com.example.calendar.ui.BackgroundTransition
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_layout)
        get_permissions()

        /*val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "day_events_database"
        ).build()
        val dao = db.getDayEventDao()

        CoroutineScope(Dispatchers.IO).launch() {
            dao.insertNewEvent(DayEvents(dayDate = 12345, eventText = "1111", sumChange = 1111.toFloat(), id = 0))
        }*/
        CoroutineScope(Dispatchers.Main).launch(Dispatchers.Main.immediate) {
            val permissionLayout = findViewById<ConstraintLayout>(R.id.mainLayout)
            BackgroundTransition(permissionLayout, 2000).animate()
        }
    }

    val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                // Permission is granted. Continue the action or workflow in your
                // app.
            } else {
                // Explain to the user that the feature is unavailable because the
                // feature requires a permission that the user has denied. At the
                // same time, respect the user's decision. Don't link to system
                // settings in an effort to convince the user to change their
                // decision.
            }
        }


    fun get_permissions() {
        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CALENDAR
            ) == PackageManager.PERMISSION_GRANTED -> {
                // You can use the API that requires the permission.
            }
            //shouldShowRequestPermissionRationale(...) -> {
            // In an educational UI, explain to the user why your app requires this
            // permission for a specific feature to behave as expected, and what
            // features are disabled if it's declined. In this UI, include a
            // "cancel" or "no thanks" button that lets the user continue
            // using your app without granting the permission.
            //showInContextUI(...)
            //}
            else -> {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                requestPermissionLauncher.launch(
                    Manifest.permission.READ_CALENDAR

                )
            }
        }
    }
}
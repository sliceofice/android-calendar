package com.example.calendar.database.entities

import androidx.room.*


@Entity(
    tableName = "DayEvents"
)

class DayEvents(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "day_date") val dayDate: Long,
    @ColumnInfo(name = "day_month") val dayMonth: Long,
    @ColumnInfo(name = "day_year") val dayYear: Long,
    @ColumnInfo(name = "event_text") val eventText: String,
    @ColumnInfo(name = "sum_change") val sumChange: Float
)

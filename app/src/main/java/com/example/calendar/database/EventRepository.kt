package com.example.calendar.database

import com.example.calendar.database.entities.DayEvents
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.Year

class EventRepository(private val dayEventsDao: DayEventsDao) {
    suspend fun insertNewEventData(statisticDbEntity: DayEvents) {
        withContext(Dispatchers.IO) {
            dayEventsDao.insertNewEvent(statisticDbEntity)
        }
    }

    suspend fun getAllEventData(month: Int, year: Int): List<SumsByDaysTuple> {
        return withContext(Dispatchers.IO) {
            return@withContext dayEventsDao.getAllEventsInMonth(month,year)
        }
    }

    suspend fun getAllEventDataInDay(day: Int, month: Int, year: Int): List<EventsTuple> {
        return withContext(Dispatchers.IO) {
            return@withContext dayEventsDao.getAllEventsInDay(day, month, year)
        }
    }

    suspend fun removeEventDataById(id: Long) {
        withContext(Dispatchers.IO) {
            dayEventsDao.deleteEvent(id)
        }
    }
}
package com.example.calendar.database

import androidx.room.ColumnInfo

data class EventsTuple (
    val id: Long,
    @ColumnInfo(name = "day_date") val dayDate: Long,
    @ColumnInfo(name = "day_month") val dayMonth: Long,
    @ColumnInfo(name = "day_year") val dayYear: Long,
    @ColumnInfo(name = "event_text") val eventText: String,
    @ColumnInfo(name = "sum_change") val sumChange: Float,
)

data class SumsByDaysTuple (
    @ColumnInfo(name = "day_date") val dayDate: Long,
    @ColumnInfo(name = "sum") val sums: Float,
)
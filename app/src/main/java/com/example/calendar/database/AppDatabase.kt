package com.example.calendar.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.calendar.database.entities.DayEvents

@Database(
    version = 1,
    entities = [
        DayEvents::class,
    ]
)
abstract class AppDatabase: RoomDatabase() {
    abstract fun getDayEventDao(): DayEventsDao
}
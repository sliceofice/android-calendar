package com.example.calendar.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.calendar.database.entities.DayEvents

@Dao
interface DayEventsDao {
    @Insert(entity = DayEvents::class)
    fun insertNewEvent(dayEventsEntity: DayEvents)

    @Query("SELECT day_date, sum(sum_change) sum FROM DayEvents where day_month = :month and day_year = :year group by day_date,day_month,day_year")
    fun getAllEventsInMonth(month: Int, year: Int): List<SumsByDaysTuple>

    @Query("SELECT * FROM DayEvents where day_date = :day and day_month = :month and day_year = :year")
    fun getAllEventsInDay(day: Int, month: Int, year: Int): List<EventsTuple>

    @Query("DELETE FROM DayEvents WHERE id = :eventId")
    fun deleteEvent(eventId: Long)
}
package com.example.calendar.database.entities

data class DayEventsConvert(
    val dayDate: Long,
    val dayMonth: Long,
    val dayYear: Long,
    val eventText: String,
    val sumChange: Float
) {

    fun toEventsEntity(): DayEvents = DayEvents(
        id = 0,
        dayDate = dayDate,
        dayMonth = dayMonth,
        dayYear = dayYear,
        eventText = eventText,
        sumChange = sumChange
    )
}
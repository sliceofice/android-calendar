package com.example.calendar

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.calendar.database.AppDatabase
import com.example.calendar.database.DayEventsDao
import com.example.calendar.database.EventRepository
import com.example.calendar.database.SumsByDaysTuple
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.Date

class LaCalendar : Fragment(), GvAdapter.ItemClickListener {

    // Таблица
    private val adapter = GvAdapter(this)
    private var gvRecycler: RecyclerView? = null

    // Шапка таблицы
    private val header_adapter = HeaderAdapter()
    private var header_Recycler: RecyclerView? = null

    private var active_day: Int = Date().date
    private var active_month: Int = Date().month
    private var active_year: Int = Date().year
    private var dao: DayEventsDao? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.supportFragmentManager?.setFragmentResultListener(
            "toCalendar",
            this
        ) { key, bundle ->
            val result = bundle.getString("status")
            if (result == "updated") {
                dao?.let {
                    runBlocking {
                        var data: List<SumsByDaysTuple>? = null
                        val query = launch {
                            data = EventRepository(it).getAllEventData(active_month + 1, active_year + 1900)
                        }
                        query.join()
                        data?.let {
                            adapter.addData(it)
                        }
                    }
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_la_calendar, container, false)
    }

    override fun onItemClick(day: Date) {
        active_year = day.year
        active_month = day.month
        active_day = day.date
        val mon_day = Date(active_year, active_month, active_day)
        adapter.setDateCal(mon_day)
        val result = mon_day.toString()
        activity?.supportFragmentManager?.setFragmentResult(
            "fromCalendar",
            bundleOf("actual_date" to result)
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init_grid_head()
        init_grid()
        set_month_year_string()
        val button_left: Button = view.findViewById(R.id.buttonLeft)
        button_left.setOnClickListener {
            button_left_on_click()
        }
        val button_right: Button = view.findViewById(R.id.buttonRight)
        button_right.setOnClickListener {
            button_right_on_click()
        }
        dao = init_db()
        dao?.let {
            runBlocking {
                var data: List<SumsByDaysTuple>? = null
                val query = launch {
                    data = EventRepository(it).getAllEventData(active_month + 1, active_year + 1900)
                }
                query.join()
                data?.let {
                    adapter.addData(it)
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun set_month_year_string() {
        val tv_date: TextView? = view?.findViewById(R.id.tvDate)
        tv_date?.setText(calc_month_text(active_month) + ' ' + calc_year_text(active_year + 1900))
    }

    fun calc_month_text(day: Int): String {
        var month = ""
        when (day) {
            0 -> month = "Январь"
            1 -> month = "Феврать"
            2 -> month = "Март"
            3 -> month = "Апрель"
            4 -> month = "Май"
            5 -> month = "Июнь"
            6 -> month = "Июль"
            7 -> month = "Август"
            8 -> month = "Сентябрь"
            9 -> month = "Октябрь"
            10 -> month = "Ноябрь"
            11 -> month = "Декабрь"
            else -> {
                print("")
            }
        }
        return month
    }

    fun calc_year_text(year: Int): String {
        return year.toString()
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun init_grid_head() {
        // Шапка
        header_Recycler = view?.findViewById(R.id.rvGridHead)
        val head_layoutManager = GridLayoutManager(view?.context, 7)
        header_Recycler?.setLayoutManager(head_layoutManager)
        header_Recycler?.setAdapter(header_adapter)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun init_grid() {
        // Таблица
        gvRecycler = view?.findViewById(R.id.rvGrid)
        val mon_day = Date(active_year, active_month, active_day)
        adapter.setDateCal(mon_day)
        val layoutManager = GridLayoutManager(view?.context, 7)
        var dividerItemDecoration = DividerItemDecoration(view?.context, RecyclerView.HORIZONTAL)
        dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.divider_drawable))
        gvRecycler?.addItemDecoration(dividerItemDecoration)
        dividerItemDecoration = DividerItemDecoration(view?.context, RecyclerView.VERTICAL)
        dividerItemDecoration.setDrawable(resources.getDrawable(R.drawable.divider_drawable))
        gvRecycler?.addItemDecoration(dividerItemDecoration)
        gvRecycler?.setLayoutManager(layoutManager)
        gvRecycler?.setAdapter(adapter)
    }

    private fun button_left_on_click() {
        active_month = active_month - 1
        if (active_month < 0) {
            active_month = 11
            active_year = active_year - 1
        }
        dao?.let {
            runBlocking {
                var data: List<SumsByDaysTuple>? = null
                val query = launch {
                    data = EventRepository(it).getAllEventData(active_month + 1, active_year + 1900)
                }
                query.join()
                data?.let {
                    adapter.addData(it)
                    set_month_year_string()
                    val mon_day = Date(active_year, active_month, 15)
                    val result = mon_day.toString()
                    activity?.supportFragmentManager?.setFragmentResult(
                        "fromCalendar",
                        bundleOf("actual_date" to result)
                    )
                    adapter.setDateCal(mon_day)
                }
            }
        }
    }

    fun init_db(): DayEventsDao? {
        view?.let {
            val db = Room.databaseBuilder(
                it.context,
                AppDatabase::class.java, "day_events_database"
            ).build()
            val dao = db.getDayEventDao()
            return dao
        }
        return null
    }

    private fun button_right_on_click() {
        active_month = active_month + 1
        if (active_month > 11) {
            active_month = 0
            active_year = active_year + 1
        }
        dao?.let {
            runBlocking {
                var data: List<SumsByDaysTuple>? = null
                val query = launch {
                    data = EventRepository(it).getAllEventData(active_month + 1, active_year + 1900)
                }
                query.join()
                data?.let {
                    adapter.addData(it)
                    set_month_year_string()
                    val mon_day = Date(active_year, active_month, 15)
                    val result = mon_day.toString()
                    activity?.supportFragmentManager?.setFragmentResult(
                        "fromCalendar",
                        bundleOf("actual_date" to result)
                    )
                    adapter.setDateCal(mon_day)
                }
            }
        }

    }
}
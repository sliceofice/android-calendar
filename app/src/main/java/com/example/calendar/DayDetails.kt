package com.example.calendar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.calendar.database.AppDatabase
import com.example.calendar.database.DayEventsDao
import com.example.calendar.database.EventRepository
import com.example.calendar.database.EventsTuple
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.Date

class DayDetails : Fragment(), DayEventsAdapter.ItemClickListener {

    private val adapter = DayEventsAdapter(this)
    private var dayEventsRecycler: RecyclerView? = null
    private var dao: DayEventsDao? = null
    private var activeDate: Date = Date()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.supportFragmentManager?.setFragmentResultListener("fromCalendar",this ) { key, bundle ->
            val result = bundle.getString("actual_date")
            activeDate = Date(result)
            dao?.let {
                runBlocking {
                    var data: List<EventsTuple>? = null
                    val query = launch {data = EventRepository(it).getAllEventDataInDay(activeDate.date,activeDate.month+1,activeDate.year+1900) }
                    query.join()
                    data?.let {
                        adapter.addData(it)
                    }
                }
            }
            val res = activeDate.toString()
            setFragmentResult("fromDayDetails", bundleOf("actual_date" to res))
        }
        setFragmentResultListener(
            "fromAddEvent"
        ) { key, bundle ->
            val result = bundle.getString("actual_date")
            activeDate = Date(result)
            dao?.let {
                runBlocking {
                    var data: List<EventsTuple>? = null
                    val query = launch {data = EventRepository(it).getAllEventDataInDay(activeDate.date,activeDate.month+1,activeDate.year+1900) }
                    query.join()
                    data?.let {
                        adapter.addData(it)
                    }
                }
            }
            val res = activeDate.toString()
            setFragmentResult("fromDayDetails", bundleOf("actual_date" to res))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_day_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val button_add_to_day: Button = view.findViewById(R.id.button_top)
        button_add_to_day.setOnClickListener {
            view.findNavController().navigate(R.id.action_dayDetails_to_addEvent)
        }
        init_rv()
        dao = init_db()
        dao?.let {
            runBlocking {
                var data: List<EventsTuple>? = null
                val query = launch {data = EventRepository(it).getAllEventDataInDay(activeDate.date,activeDate.month+1,activeDate.year+1900) }
                query.join()
                data?.let {
                    adapter.addData(it)
                }
            }
        }


    }

    override fun onItemClick(dayEvent: EventsTuple) {
        dao?.let {
            runBlocking {
                var data: List<EventsTuple>? = null
                var query = launch {EventRepository(it).removeEventDataById(dayEvent.id) }
                query.join()
                query = launch {data = EventRepository(it).getAllEventDataInDay(activeDate.date,activeDate.month+1,activeDate.year+1900) }
                query.join()
                data?.let {
                    adapter.addData(it)
                    val result = "updated"
                    activity?.supportFragmentManager?.setFragmentResult(
                        "toCalendar",
                        bundleOf("status" to result)
                    )
                }
            }
        }
    }

    fun init_rv() {
        dayEventsRecycler = view?.findViewById(R.id.rv_top)
        val layoutManager = LinearLayoutManager(view?.context)
        dayEventsRecycler?.setLayoutManager(layoutManager)
        dayEventsRecycler?.setAdapter(adapter)
    }

    fun init_db(): DayEventsDao? {
        view?.let {
            val db = Room.databaseBuilder(
                it.context,
                AppDatabase::class.java, "day_events_database"
            ).build()
            val dao = db.getDayEventDao()
            return dao
        }
        return null
    }
}